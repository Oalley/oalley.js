var inside = require('point-in-polygon')
var polyline = require('@mapbox/polyline')
var request = require('superagent')

function Oalley (apiKey) {
  this.apiKey = apiKey
}

Oalley.prototype.filterData = function (dataset, polygons) {
  var returnedData = []
  for (var i = 0; i < dataset.length; i++) {
    var lat = dataset[i].lat
    var lng = dataset[i].lng

    if (isNaN(lat) || isNaN(lng)) { throw new Error('Latitude or longitude is not a number') }

    for (var j = 0; j < polygons.length; j++) {
      var poly = polygons[j]
      if (inside([lat, lng], poly)) {
        returnedData.push(dataset[i])
        break
      }
    }
  }
  return returnedData
}

Oalley.prototype.getJSON = function (url, config, cb) {
  request.get(url)
    .query(config)
    .end((err, res) => {
      if (err) { return cb(err) }
      let encodedPolygons = res.body.polyline

      let polygons = []
      for (var i = 0; i < encodedPolygons.length; i++) {
        polygons.push(polyline.decode(encodedPolygons[i]))
      }
      return cb(null, polygons)
    })
}

Oalley.prototype.isochrone = function (config, cb) {
  var url = 'https://api.oalley.fr/api/AppKeys/' + this.apiKey + '/isochrone'
  this.getJSON(url, config, cb)
}

Oalley.prototype.isodistance = function (config, cb) {
  var url = 'https://api.oalley.fr/api/AppKeys/' + this.apiKey + '/isodistance'
  this.getJSON(url, config, cb)
}

Oalley.prototype.isochroneFilter = function (config, dataset, cb) {
  let self = this
  this.isochrone(config, function (err, data) {
    if (err) { cb(err) }

    cb(null, self.filterData(dataset, data))
  })
}

Oalley.prototype.isodistanceFilter = function (config, dataset, cb) {
  let self = this
  this.isodistance(config, function (err, data) {
    if (err) { cb(err) }

    cb(null, self.filterData(dataset, data))
  })
}

module.exports = Oalley
