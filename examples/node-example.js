let Oalley = require('../main.js')

let oalley = new Oalley('P5_KumUf8nilu3jq_16RYXCKdlnfjzFs_F6fW8aB2CQ')

let config = {
  'mode': 'car',
  'duration': 3600,
  'lat': 43.60,
  'lng': 1.441
}

let dataset = [{
  'lat': 43.60,
  'lng': 1.441
},
{
  'lat': 33.60,
  'lng': 2.441
}
]

oalley.isochroneFilter(config, dataset, function (err, data) {
  if (err) { throw err }
  console.log('Isochrone data filtered:', data)


  let config = {
    'mode': 'car',
    'distance': 30000,
    'lat': 43.60,
    'lng': 1.441
  }

  oalley.isodistanceFilter(config, dataset, function (err, data) {
    if (err) { throw err }
    console.log('isodistance data filtered:', data)
  })
})
