# Oalley javascript API

Oalley.js is a simple library for using Oalley API in nodejs and browser.

Library can compute isochrone and isodistance area and can filter data inside those area.

![oalley.js library demo](img/example.png)


## Install

### Nodejs
```
npm install oalley.js
```

### Browser

Include `dist/oalley.min.js` in your code like:
```
  <script src="../dist/oalley.js"></script>
```

## Docs

Set the library with your api key :

```
let oalley = new Oalley('YOUR-API-KEY')
```

Each function of the library take a configuration object :
```
let config = {
  'mode': Transport Mode
  'duration': Travel duration in seconds (only for isochrone)
  'distance': Travel distance in meters (only for isodistance)
  'lat': Latitude of the center of the isoarea
  'lng': Longitude of the center of the isoarea
}
```
Simple example of a isochrone configuration :

```
let config = {
  'mode': 'car',
  'duration': 3600,
  'lat': 43.60,
  'lng': 1.441
}
```
https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1

List of methods:

* `oalley.isochrone(config, cb)`: Compute isochrone area
* `oalley.isodistance(config, cb)`: Compute isodistance area
* `oalley.isochroneFilter(config, dataset, cb)`: Return data which are in the isochrone area (for dataset see Filtering data inside isoarea section)
* `oalley.isodistanceFilter(config, dataset, cb)`: Return data which are in the isodistance area (for dataset see Filtering data inside isoarea section)

### Filtering data inside isoarea

The library can filter geodata inside an isochrone or an isodistance with the `isochroneFilter` and `isodistanceFilter`.
In order to do that a dataset is passed to these methods representing a list of dictionary. Each element of the dictionary have to contain at least `lat` and `lng`  keys.

Here below a simple example of a dataset:

```

let dataset = [{
  'lat': 43.60,
  'lng': 1.441,
  'mydata': 42
},
{
  'lat': 33.60,
  'lng': 2.441,
  'mydata': 24
}
]
```

## Examples
All examples are available in the `examples` folder.

** Do not forget to set your API Key **

A nodejs example is available in the file `node-example.js`

A browser example is available in the file `browser-example.html`
